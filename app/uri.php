<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 22/11/18
 * Time: 14:17
 */

function geturi(){
    $base = explode('/', $_SERVER['REQUEST_URI']);
    $urlDir = ['', 'ticket', 'administration', 'Ajax', "bd", "css", "doc", "historique", "image_ticket", "images", "js", "php", "struct", "ticket"];
    $uri = "";

    foreach ($base as $key => $item){
        if ($item === ''){
            unset($base[$key]);
        }elseif (strpos($item, '.php') !== false){
            unset($base[$key]);
        }elseif (array_search($item,$urlDir) == 0){
            $uri = $uri . '/' . $item;
        }
    }

    return $uri;
}

$base = geturi();
$uri = $base;

