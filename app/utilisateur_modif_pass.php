<?php
/**
 * Created by IntelliJ IDEA.
 * User: Alexandre Debris
 * Date: 09/11/18
 * Time: 16:22
 */


/*
 * Inclusion(s)
 * ------------
 * ~ Session
 * ~ Fonction test_input($data){}
 * ~ Snack bar (message d'info)
 */
include_once "struct/session.php";
include_once "php/test_input.php";
include_once "php/snackbar.php";
require_once "uri.php";

// Déclaration des variables à vide
$id = $mdp = $verif_mdp = "";
$id_err = $mdp_err = FALSE;


// Si l'utilisateur à envoyer le formulaire
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    /*
     * Identifiant
     * -----------
     * ~ Requis
     * ~ Pas de condition de validation particulière
     */
    if (empty($_POST["password"])) {
        $id_err = TRUE;
    } else {
        $mdp = test_input($_POST["password"]);
    }

    /*
     * Mot de passe
     * ------------
     * ~ Requis
     * ~ Pas de condition de validation particulière
     */
    if (empty($_POST["verif_password"])) {
        $mdp_err = TRUE;
    } else {
        $verif_mdp = test_input($_POST["verif_password"]);
    }


    if (!($id_err && $mdp_err) && $mdp === $verif_mdp) {
        // Inclusion de l'instance de l'objet PDO
        include_once('php/t_connex_bd.php');

        $pass_hash = password_hash($verif_mdp, PASSWORD_DEFAULT);

        $id = $_SESSION['usr_connected']['id'];

        $sql = "UPDATE UTILISATEUR SET PASS_CONNEX = '".$pass_hash."', MODIF_PASS = '0' WHERE ID = '".$id."';";

        try {
            $bdd->exec($sql);
            $valide = true;
        } catch (PDOException $pdoe) {
            $snack = "Erreur interne, impossible de modifier le mot de passe.";
        }

        if ($valide === true){
            header('Location: accueil.php');
            exit();
        }

    } else {
        $snackbar = "Information incorrect !";
    }

}

?>
<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <?php

        // Inclusion des éléments placé dans la balise <head>
        include_once("struct/head.php");
        $titre_page = "Connexion".$title; ?>
        <title><?= $titre_page ?></title>


        <link rel="stylesheet" href="<?= $base ?>/css/connexion.css">
    </head>
    <body>
        <div id="page">
            <?php

            // Inclusion de l'en-tête
            include_once "struct/header.html";

            ?>
            <section>
                <div class="container">
                    <h2>Bienvenue sur l'application de gestion de l'atelier</h2>
                    <p>Pour commencer, authentifiez vous</p>
                    <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                        <?= isset($snackbar) ? "<p class='error-text' style='text-align: center'>".$snackbar."</p>" : "" ?>
                        <div>
                            <label for="password">Nouveau mot de passe</label>
                            <input class="<?= isset($snackbar) ? "error-input" : "" ?>" type="password" id="password"
                                   name="password" placeholder="Mot de passe"
                                   value="<?php echo $mdp ?>" required>
                            <span><?php //if($id_err) echo "Identifiant vide ou incorrect"; ?></span>
                        </div>
                        <div>
                            <label for="verif_password">Verification du mot de passe</label>
                            <input class="<?= isset($snackbar) ? "error-input" : "" ?>" type="password"
                                   id="verif_password"
                                   name="verif_password" placeholder="Verification mot de passe"
                                   value="<?php echo $verif_mdp ?>" required>
                            <span><?php //if($mdp_err) echo "Mot de passe vide ou incorrect" ?></span>
                        </div>
                        <input id="Bconnex" class="button" type="submit" name="Connexion" value="Connexion">
                    </form>
                </div>
            </section>
            <?php

            // Inclusion du pied de page (footer)
            include_once "struct/footer.php";

            ?>

        </div>
    </body>
</html>