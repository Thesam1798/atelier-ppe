ALTER TABLE  `TICKET` CHANGE  `TITRE`  `TITRE` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE TICKET
ADD D_CLOTURE date;

CREATE TABLE IF NOT EXISTS HISTORIQUE(
  ID INTEGER(4) NOT NULL  ,
  RESPONSABLE VARCHAR(25) ,
  CATEGORIE varchar(128)  ,
  LIEU varchar(128)  ,
  STATUT varchar(25) ,
  CREATEUR varchar(25) ,
  TITRE VARCHAR(50) ,
  DESCRIPTION VARCHAR(255)  ,
  D_OUVERTURE DATE NULL  ,
  H_OUVERTURE TIME NULL ,
  D_CLOTURE DATE NULL ,
  PRIMARY KEY (ID) 
)