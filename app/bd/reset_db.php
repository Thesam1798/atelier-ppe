<?php
/**
 * Created by IntelliJ IDEA
 * Project  : Atelier
 * File     : reset_db.php
 * User     : Alexandre Debris
 * Date     : 22/05/2019
 * Time     : 15:51
 */

if (isset($_GET['verify'])) {
    require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'php' . DIRECTORY_SEPARATOR . 't_connex_bd.php';

    $bdd->exec("SET FOREIGN_KEY_CHECKS=0;");

    $bdd->exec("DROP DATABASE IF EXISTS $PARAM_nom_bd; CREATE DATABASE $PARAM_nom_bd; USE $PARAM_nom_bd;");

    $file = file_get_contents(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'bd' . DIRECTORY_SEPARATOR . 'create_database.sql', true);

    $bdd->exec($file);

    $file = file_get_contents(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'bd' . DIRECTORY_SEPARATOR . 'data.sql', true);
    $bdd->exec($file);

    $bdd->exec("SET FOREIGN_KEY_CHECKS=1;");

    echo "DateBase restored";

} else {
    echo "Error not verify";
}
