create or replace table ACTION
(
    NUM int(4) auto_increment
        primary key,
    NOM varchar(128) null
)
    charset=latin1;

create or replace table CATEGORIE
(
    NUM int(4) auto_increment
        primary key,
    NOM varchar(128) null
)
    charset=latin1;

create or replace table CLASSE
(
    NUM int(4) auto_increment
        primary key,
    NOM varchar(128) null
)
    charset=latin1;

create or replace table IMPORTANCE
(
    NUM int(4) auto_increment
        primary key,
    NOM varchar(128) null
)
    charset=latin1;

create or replace table LIEU
(
    NUM int(4) auto_increment
        primary key,
    NOM varchar(128) null
)
    charset=latin1;

create or replace table STATUT
(
    NUM int(4) auto_increment
        primary key,
    NOM varchar(128) null
)
    charset=latin1;

create or replace table UTILISATEUR
(
    ID int(4) auto_increment
        primary key,
    NUM_CLASSE int(4) not null,
    NOM varchar(64) null,
    PRENOM varchar(64) null,
    TEL int(4) null,
    MAIL varchar(128) null,
    ID_CONNEX varchar(128) null,
    PASS_CONNEX varchar(255) null,
    MODIF_PASS int(1) default 0 null,
    FONCTION varchar(64) null,
    constraint UTILISATEUR_ibfk_1
        foreign key (NUM_CLASSE) references CLASSE (NUM)
)
    charset=latin1;

create or replace table TICKET
(
    ID int(4) auto_increment
        primary key,
    ID_RESPONSABLE int(4) null,
    NUM_CATEGORIE int(4) not null,
    NUM_LIEU int(4) not null,
    NUM_STATUT int(4) not null,
    ID_CREATEUR int(4) not null,
    NUM_IMPORTANCE int(4) not null,
    PAR_PERSONNEL int(1) default 0 null,
    TITRE varchar(50) null,
    DESCRIPTION varchar(255) null,
    D_OUVERTURE date null,
    H_OUVERTURE time null,
    D_CLOTURE date null,
    VALIDER int(1) default 0 null,
    constraint TICKET_ibfk_1
        foreign key (ID_RESPONSABLE) references UTILISATEUR (ID),
    constraint TICKET_ibfk_2
        foreign key (NUM_CATEGORIE) references CATEGORIE (NUM),
    constraint TICKET_ibfk_3
        foreign key (NUM_LIEU) references LIEU (NUM),
    constraint TICKET_ibfk_4
        foreign key (NUM_STATUT) references STATUT (NUM),
    constraint TICKET_ibfk_5
        foreign key (ID_CREATEUR) references UTILISATEUR (ID),
    constraint TICKET_ibfk_6
        foreign key (NUM_IMPORTANCE) references IMPORTANCE (NUM)
)
    charset=latin1;

create or replace table COMMENTAIRE
(
    ID_TICKET int(4) not null,
    ID int(4) not null,
    ID_UTILISATEUR int(4) not null,
    CONTENU varchar(255) null,
    H_COM time null,
    D_COM date null,
    primary key (ID_TICKET, ID),
    constraint COMMENTAIRE_ibfk_1
        foreign key (ID_UTILISATEUR) references UTILISATEUR (ID),
    constraint COMMENTAIRE_ibfk_2
        foreign key (ID_TICKET) references TICKET (ID)
)
    charset=latin1;

create or replace index I_FK_COMMENTAIRE_TICKET
    on COMMENTAIRE (ID_TICKET);

create or replace index I_FK_COMMENTAIRE_UTILISATEUR
    on COMMENTAIRE (ID_UTILISATEUR);

create or replace table EVOLUTION
(
    ID_TICKET int(4) not null,
    ID char(32) not null,
    ID_UTILISATEUR int(4) not null,
    NUM_ACTION int(4) not null,
    CHAMP varchar(128) null,
    LIB_CHAMP varchar(255) null,
    H_EVO time null,
    D_EVO date null,
    primary key (ID_TICKET, ID),
    constraint EVOLUTION_ibfk_1
        foreign key (ID_UTILISATEUR) references UTILISATEUR (ID),
    constraint EVOLUTION_ibfk_2
        foreign key (ID_TICKET) references TICKET (ID),
    constraint EVOLUTION_ibfk_3
        foreign key (NUM_ACTION) references ACTION (NUM)
)
    charset=latin1;

create or replace index I_FK_EVOLUTION_ACTION
    on EVOLUTION (NUM_ACTION);

create or replace index I_FK_EVOLUTION_TICKET
    on EVOLUTION (ID_TICKET);

create or replace index I_FK_EVOLUTION_UTILISATEUR
    on EVOLUTION (ID_UTILISATEUR);

create or replace index I_FK_TICKET_CATEGORIE
    on TICKET (NUM_CATEGORIE);

create or replace index I_FK_TICKET_IMPORTANCE
    on TICKET (NUM_IMPORTANCE);

create or replace index I_FK_TICKET_LIEU
    on TICKET (NUM_LIEU);

create or replace index I_FK_TICKET_STATUT
    on TICKET (NUM_STATUT);

create or replace index I_FK_TICKET_UTILISATEUR
    on TICKET (ID_RESPONSABLE);

create or replace index I_FK_TICKET_UTILISATEUR1
    on TICKET (ID_CREATEUR);

create or replace index I_FK_UTILISATEUR_CLASSE
    on UTILISATEUR (NUM_CLASSE);

create or replace definer = user@`%` trigger Trigger_Email_BeforeInsert
    before INSERT on UTILISATEUR
    for each row
begin

    if EXISTS(SELECT * FROM UTILISATEUR WHERE MAIL = NEW.MAIL)
    then
        SIGNAL sqlstate '45001' set message_text = "The email already exists";
    end if;

end;

create or replace definer = user@`%` trigger Trigger_Email_BeforeUpdate
    before UPDATE on UTILISATEUR
    for each row
begin

    if EXISTS(SELECT * FROM UTILISATEUR WHERE MAIL = NEW.MAIL)
    then
        SIGNAL sqlstate '45001' set message_text = "The email already exists";
    end if;

end;

