<?php 
/**
 * Cette page contient les éléments HTML qui seront placé dans l'élement <head> des pages.
 *
 * @author Anthony Lozano (2015)
 * @version  1.0.0
 */

require_once dirname(dirname(__FILE__)).'/uri.php';

$title = " - Atelier";
?>
<meta charset="UTF-8">
<link rel="icon" href="<?= $uri ?>/favicon.png" type="image/png" sizes="32x32">
<link rel="stylesheet" media="screen" type="text/css"  href="<?= $uri ?>/css/header.css">
<link rel="stylesheet" media="screen" type="text/css" href="<?= $uri ?>/css/style.css">
<link rel="stylesheet" media="print" type="text/css" href="<?= $uri ?>/css/print.css">
<script type="text/javascript" src="<?= $uri ?>/js/jquery-1.11.2.js"></script>