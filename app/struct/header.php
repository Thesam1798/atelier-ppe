<?php
/**
 * En-tête
 *
 * @author Emilie Graton (V. 1 - 2012)
 * @author Anthony Lozano (2015)
 *
 * @version  2.0.4
 */

require_once dirname(dirname(__FILE__)).'/uri.php';

/**
 * Créé une icône de navigation (iône + label)
 * @param string $label Label placé sous l'icône
 * @param string $icone Nom de l'icône se trouvant dans /atelier_v3/images/iconnav/
 * @param string $action Chemin (lien) vers la page désirée
 * @return string Eléments HTML permettant d'afficher l'icône de navigation (à appeler dans l'en-tête)
 */
function createIconnav($label, $icone, $action)
{

    $base = explode('/', (pathinfo($_SERVER["REQUEST_URI"])["dirname"]));

    $urlDir = ['', 'ticket', 'administration', 'Ajax', "bd", "css", "doc", "historique", "image_ticket", "images", "js", "php", "struct", "ticket"];

    if (count($base) > 1) {
        if (count($base) == 2 && in_array($base[1], $urlDir)) {
            $base = "";
        } elseif ($base[1] !== '') {
            $base = "/".$base[1];
        } elseif (count($base) > 2) {
            for ($i = 1; $i <= count($base) + 1; $i++) {
                if (isset($base[$i + 1]) && isset($base[$i])) {
                    if ((in_array($base[$i + 1], $urlDir))) {
                        $real_base = "/";

                        for ($a = 1; $a <= $i; $a++) {
                            if (isset($base[$a])) {
                                $real_base = $real_base.$base[$a]."/";
                            }
                        }
                    }
                }
            }
        }
        $base = isset($real_base) == false ? $base : $real_base;
    }

    $iconnav = '<li class="iconnav">'."\n"
        .'<a href="'.$action.'">'."\n"
        .'<img src="'.$base.'/images/iconnav/'.$icone.'" alt="'.$label.'">'."\n"
        .'<span>'.$label.'</span>'."\n"
        .'</a>'."\n"
        .'</li>';
    return $iconnav;
}

$nom_header = $_SESSION['usr_connected']['nom'];
$prenom_header = $_SESSION['usr_connected']['prenom'];
$tdc = $_SESSION['usr_connected']['tdc'];


/**
 * @var int $classe Définit la classe de l'utilisateur, afin de déterminer les éléments du menu à afficher
 * $classe == 1 -> Administrateur
 * $classe == 2 -> Agent chef
 * $classe == 3 -> Agent
 */
if (isset($_SESSION['usr_connected']['classe'])) {
    $classe = $_SESSION['usr_connected']['classe'];
} else {
    $classe = 0;
}

// Création des icones
// -------------------

$menu = "";

if ($classe == 1) {

    // Tickets
    $menu = '<div class="vr">'
        .'<span class="h">'."Tickets".'</span>'
        .createIconnav("Créer", "add.png", $base."/ticket/creer_ticket.php")
        .createIconnav("Validation", "fiche.svg", $base."/ticket/lister_ticket_validation.php")
        .createIconnav("Attribuer", "help.png", $base."/ticket/lister_ticket_attribuer.php")
        .createIconnav("En cours", "curr.svg", $base."/ticket/lister_ticket_encours.php")
        .createIconnav("Résolu", "check.png", $base."/ticket/lister_ticket_resolu.php")
        .createIconnav("Tous", "all.png", $base."/ticket/lister_ticket_tous.php")
        .'</div>';

    // Historique
    $menu .= '<div class="vr">'
        .'<span class="h">'."&nbsp".'</span>'
        .createIconnav("Historique", "histo.svg", $base."/historique/lister_ticket_historique.php")
        .'</div>';

    // Admin
    $menu .= '<div class="vr">'
        .'<span class="h">'."Administration".'</span>'
        .createIconnav("Utilisateurs", "pers.svg", $base."/administration/utilisateurs.php")
        .createIconnav("Lieux", "place.png", $base."/administration/lieu.php")
        .createIconnav("Catégories", "cat.png", $base."/administration/categorie.php")
        //.createIconnav("Purger données", "stor.png", $base."/administration/purger_bdd.php")
        .'</div>';

} elseif ($classe == 2) {

    // Tickets
    $menu = '<div class="vr">'
        .'<span class="h">'."Tickets".'</span>'
        .createIconnav("Créer", "add.png", $base."/ticket/creer_ticket.php")
        .createIconnav("Attribuer", "help.png", $base."/ticket/lister_ticket_attribuer.php")
        .createIconnav("En cours", "curr.svg", $base."/ticket/lister_ticket_encours.php")
        .createIconnav("Résolu", "check.png", $base."/ticket/lister_ticket_resolu.php")
        .createIconnav("Tous", "all.png", $base."/ticket/lister_ticket_tous.php")
        .'</div>';

    // Historique
    $menu .= '<div class="vr">'
        .'<span class="h">'."&nbsp".'</span>'
        .createIconnav("Historique", "histo.svg", $base."/historique/lister_ticket_historique.php")
        .'</div>';

} elseif ($classe == 3) {

    // Tickets
    $menu = '<div class="vr">'
        .'<span class="h">'."Tickets".'</span>'
        .createIconnav("En cours", "curr.svg", $base."/ticket/lister_ticket_encours.php")
        .createIconnav("Résolu", "check.png", $base."/ticket/lister_ticket_resolu.php")
        .createIconnav("Tous", "all.png", $base."/ticket/lister_ticket_tous.php")
        .'</div>';

} elseif ($classe == 4) {

    // Tickets
    $menu = '<div class="vr">'
        .'<span class="h">'."Tickets".'</span>'
        .createIconnav("En cours", "curr.svg", $base."/ticket/lister_ticket_encours.php")
        .'</div>';

} elseif ($classe == 5) {

    // Tickets
    $menu = '<div class="vr">'
        .'<span class="h">'."Tickets".'</span>'
        .createIconnav("Ajouter", "curr.svg", $base."/ticket/creer_ticket_personnel.php")
        .createIconnav("List", "all.png", $base."/ticket/lister_ticket_personnel.php")
        .'</div>';

}


// Message de bienvenue
// --------------------

$welcome_usr = "Bienvenue ".$prenom_header." ".$nom_header;
$welcome_type = "Vous êtes connecté en tant qu'".$tdc;

?>
<header>
    <div id="entete">
        <div class="logo"></div><!--
	 -->
        <div class="welcome_msg">
			<span>
				<?php echo $welcome_usr; ?>
			</span>
            <span>
				<?php echo $welcome_type; ?>
			</span>
        </div><!--
	 -->
        <nav class="welcome_nav">
            <ul>
                <li class="iconnav_top"><a href="<?= $base ?>/accueil.php" title="Retour à l'accueil"><img
                                src="<?= $base ?>/images/iconnav/home.png" alt="Accueil"><span>Accueil</span></a></li><!--
			 -->
                <li class="iconnav_top"><a href="<?= $base ?>/php/t_deconnexion.php" title="Fin de la session"><img
                                src="<?= $base ?>/images/iconnav/exit.png"
                                alt="Déconnexion"><span>Déconnexion</span></a></li>
            </ul>
        </nav>
    </div>
    <hr>

    <nav id="menu">
        <ul>
            <?php echo $menu; ?>
        </ul>
    </nav>
</header>