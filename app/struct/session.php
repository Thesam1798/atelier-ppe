<?php
/**
 * 
 * @author Anthony Lozano (2015)
 *
 * @version  1.0.0
 */

session_start();

$base = explode('/', (pathinfo($_SERVER["REQUEST_URI"])["dirname"]));

$urlDir = ['', 'ticket', 'administration', 'Ajax', "bd", "css", "doc", "historique", "image_ticket", "images", "js", "php", "struct", "ticket"];

if (count($base) > 1) {
    if (count($base) == 2 && in_array($base[1], $urlDir)) {
        $base = "";
    } elseif ($base[1] !== '') {
        $base = "/".$base[1];
    } elseif (count($base) > 2) {
        for ($i = 1; $i <= count($base) + 1; $i++) {
            if (isset($base[$i + 1]) && isset($base[$i])) {
                if ((in_array($base[$i + 1], $urlDir))) {
                    $real_base = "/";

                    for ($a = 1; $a <= $i; $a++) {
                        if (isset($base[$a])) {
                            $real_base = $real_base.$base[$a]."/";
                        }
                    }
                }
            }
        }
    }
    $base = isset($real_base) == false ? $base : $real_base;
}

if (!isset($_SESSION['usr_connected'])) {
	header('location: '.$base.'/utilisateur_inconnu.php');
	exit;
}

/**
 * Met fin à la session et redirige l'utilisateur sur la page de connexion
 */
function deconnexion() {

    $base = explode('/', (pathinfo($_SERVER["REQUEST_URI"])["dirname"]));

    $urlDir = ['', 'ticket', 'administration', 'Ajax', "bd", "css", "doc", "historique", "image_ticket", "images", "js", "php", "struct", "ticket"];

    if (count($base) > 1) {
        if (count($base) == 2 && in_array($base[1], $urlDir)) {
            $base = "";
        } elseif ($base[1] !== '') {
            $base = "/".$base[1];
        } elseif (count($base) > 2) {
            for ($i = 1; $i <= count($base) + 1; $i++) {
                if (isset($base[$i + 1]) && isset($base[$i])) {
                    if ((in_array($base[$i + 1], $urlDir))) {
                        $real_base = "/";

                        for ($a = 1; $a <= $i; $a++) {
                            if (isset($base[$a])) {
                                $real_base = $real_base.$base[$a]."/";
                            }
                        }
                    }
                }
            }
        }
        $base = isset($real_base) == false ? $base : $real_base;
    }

    session_unset();
	session_destroy();
	header('Location: '.$base.'/index.php');
}

?>