<?php
/**
 * Cette page d'index contient le formulaire de connexion à l'application
 *
 * Version PHP >= 5.5.0
 *
 * @author Emilie Graton (V. 1 - 2012)
 * @author Anthony Lozano (2015)
 *
 * @version  2.0.0
 */


		include_once('php/t_connex_bd.php');

		// $sql = "SELECT ID FROM TICKET";
		$sql = "SELECT TICKET.ID, MAX(D_EVO) CLOTURE
			FROM TICKET
			INNER JOIN EVOLUTION
			ON TICKET.ID=EVOLUTION.ID_TICKET
			WHERE NUM_STATUT=5
			AND NUM_ACTION=5
			AND D_CLOTURE IS NULL
			GROUP BY TICKET.ID";
		// $sql = "SELECT * FROM TICKET WHERE NUM_STATUT=5";

		try {
			$res = $bdd->query($sql);
			$table = $res->fetchAll(PDO::FETCH_ASSOC);
		} catch(PDOException $pdoe) {
			$snack = "Erreur interne, impossible de comparer les identifiants.";
			$table = array(array());
		}


?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
	<?php

	// Inclusion des éléments placé dans la balise <head>
	include_once("struct/head.php");
	$titre_page = "Connexion". $title; ?>
	<title><?= $titre_page ?></title>

	
	<link rel="stylesheet" href="css/connexion.css">
</head>
<body>
	<div id="page">
		<section>
			<div class="container">
				<h2>Changement date cloture</h2>
				<?php $cpt = 0; ?>
				<?php foreach ($table as $row): ?>
					<?php 
					$cpt ++;
					$requete = "UPDATE TICKET SET D_CLOTURE = '".$row['CLOTURE']."' WHERE ID = ".$row['ID'].";";
					// $requete = "vide";
					echo $requete;


					try {
						$res = $bdd->exec($requete);
						// $table = $res->fetchAll(PDO::FETCH_ASSOC);
					} catch(PDOException $pdoe) {
						echo "Erreur interne, impossible de comparer les identifiants.";
						// $table = array(array());
					}
					?>
					<br>
				<?php endforeach; ?>
				<?= $cpt ?>
			</div>
		</section>
	</div>
</body>
</html>