<?php
/**
 * Gestion des utilisateurs
 *
 * @author Anthony Lozano (2015)
 *
 * @version  1.2.0
 */

/*
 * Inclusion(s)
 * ------------
 * ~ Session
 * ~ Connexion à la base
 * ~ Fonctions permettant de créer les listes HTML
 * ~ Snack bar (message d'info)
 * ~ Fonction test_input($data)
 * ~ Ajax pour la modification des utilisateur



 *password_hash ne marche qu'a partir de la version 5.5 php
 */

include_once "../struct/session.php";
include_once "../php/t_connex_bd.php";
include_once "../php/snackbar.php";
// include_once "../php/listes_formulaire.php";
include_once "../php/test_input.php";

$agent = 0;
$nom = $prenom = $classe = $fonction = $tel = $email = $id_connex = $mdp_connex = $mdpv_connex = NULL;


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    /*
    * Nouvel utilisateur a partie d'un csv
    */
    if (isset($_POST['nv_utilisateur_csv'])) {

        //Verrification du type de fichier

        if ($_FILES['fileupload']['type'] === "text/csv" || $_FILES['fileupload']['type'] === "application/vnd.ms-excel") {

            //Lecture du fichier
            $text = stream_get_contents(fopen($_FILES['fileupload']['tmp_name'], "r"));
            $text = explode("\n", $text);

            //Séparation par ligne
            foreach ($text as $id_ligne => $ligne) {
                $text[$id_ligne] = explode(",", $ligne);
            }

            //Convertis les ligne en tableaux, Ligne = [NomDeLaColone => Valeur, NomDeLaCol...]
            foreach ($text as $id_ligne => $ligne) {
                if ($id_ligne !== 0) {
                    $argument = [];
                    foreach ($ligne as $id_arg => $arg) {
                        $argument[$text[0][$id_arg]] = $arg;
                    }
                    $text[$id_ligne] = $argument;
                }
            }

            $error = false;
            $sqlInsert = [];

            //Création du SQL pour chaque ligne, avec verification et generation du mot de passe
            foreach ($text as $id => $user){
                if ($id === 0){
                    continue;
                }

                if (isset($user['Nom'])){
                    $nom = $user['Nom'];
                }else{
                    $error = "Nom Manquent";
                    break;
                }

                if (isset($user['Prenom'])){
                    $prenom = $user['Prenom'];
                }else{
                    $error = "Prenom Manquent";
                    break;
                }

                if (isset($user['Fonction'])){
                    $fonction = $user['Fonction'];
                }else{
                    $fonction = 'Non connue';
                }

                if (isset($user['Telephone'])){
                    $telephone = $user['Telephone'];
                }else{
                    $telephone = 'Non connue';
                }

                if (isset($user['Mail'])){
                    $email = $user['Mail'];
                }else{
                    $error = "Mail Manquent";
                    break;
                }

                //Generation du nom d'utilisateur et mot de passe
                $gen_username = strtolower($prenom).".".strtolower($nom);
                $gen_password = strtolower($prenom).".".strtolower($nom).".".date("Y");

                $gen_password_hash = password_hash($gen_password, PASSWORD_DEFAULT);

                $sqlInsertText = '("'.$nom.'","'.$prenom.'",5,"'.$fonction.'","'.$telephone.'","'.$email.'","'.$gen_username.'","'.$gen_password_hash.'",1)';

                //Verification de doublon
                $sql = 'SELECT * FROM UTILISATEUR WHERE ID_CONNEX = "'.$gen_username.'";';
                $res = $bdd->query($sql);
                $table = $res->fetchAll(PDO::FETCH_ASSOC);

                //Ajout si pas de doublon
                if (count($table) > 0 === false){
                    array_push($sqlInsert,$sqlInsertText);
                }

            }

            //Si une erreur est visible dans les generation
            if ($error === false){
                //verification si il a bien des personne a jouter
                if (count($sqlInsert) > 0){
                    //combine le tous en string
                    $sqlInsert = (join(",",$sqlInsert).";");

                    $sql = "INSERT INTO `UTILISATEUR`(`NOM`, `PRENOM`, `NUM_CLASSE`, `FONCTION`,`TEL`, `MAIL`, `ID_CONNEX`, `PASS_CONNEX`, `MODIF_PASS`)\n"
                        ."VALUES " . $sqlInsert;
                    $pdo_erreur = false;
                    //Excution de la request
                    try {
                        $bdd->exec($sql);
                    } catch (PDOException $pdoe) {
                        $pdo_erreur = TRUE;
                    }

                    $snackbar = $pdo_erreur ? "Désolé, mais une erreur est apparue, impossible d'ajouté l'utilisateur".'<br><span>'.$pdoe->getMessage().'</span>' : "Les utilisateur on été correctement ajouté !<br>connextion posible avec <br>Nom d'utilisatuer : prenom.nom <br>Mot de passe : prenom.nom." . date("Y");
                    $sqlInsert = NULL;
                }else{
                    $snackbar = "Désolé, mais il n'a pas d'utilisateur à rajouter.";
                }

            }else{
                $snackbar = "Désolé, mais une erreur est apparue, impossible d'ajouté les utilisateur : " . $error;
            }

        } else {
            $snackbar = "Désolé, mais il n'a pas de fichier valide.<br>Vous devez utiliser les CSV.";
        }

    }

    /*
     * Nouvel utilisateur
     */
    if (isset($_POST['nv_utilisateur'])) {

        /*
         * init
         * ----
         */

        $nom_err = $prenom_err = $classe_err = $fonction_err = $tel_err = $email_err = $id_connex_err = $mdp_connex_err = false;
        $pdo_erreur = false;

        /*
         * Nom
         * ---
         * ~ Requis
         * ~ Texte simple
         */
        if (empty($_POST["nom"])) {
            $nom_err = TRUE;
        } else {
            $nom = test_input($_POST["nom"]);
        }

        /*
         * Prenom
         * ------
         * ~ Requis
         * ~ Texte simple
         */
        if (empty($_POST["prenom"])) {
            $prenom_err = TRUE;
        } else {
            $prenom = test_input($_POST["prenom"]);
        }

        /*
         * Classe
         * ------
         * ~ Requis (liste)
         */
        $classe = test_input($_POST["classe"]);

        /*
         * Fonction
         * ------------
         * ~ Facultatif
         * ~ Texte simple
         */
        if (empty($_POST["fonction"])) {
            $fonction_err = TRUE;
        } else {
            $fonction = test_input($_POST["fonction"]);
        }

        /*
         * Telephone
         * ---------
         * ~ Facultatif
         * ~ Texte simple
         */
        if (empty($_POST["tel"])) {
            $tel_err = TRUE;
        } else {
            $tel = test_input($_POST["tel"]);
        }

        /*
         * Email
         * -----
         * ~ Facultatif
         * ~ Texte simple
         */
        if (empty($_POST["email"])) {
            $email_err = TRUE;
        } else {
            $email = test_input($_POST["email"]);
        }

        /*
         * Identifiant
         * -----------
         * ~ Facultatif
         * ~ Texte simple
         */
        if (empty($_POST["id_connex"])) {
            $id_connex_err = TRUE;
        } else {
            $id_connex = test_input($_POST["id_connex"]);
        }

        /*
         * Mot de passe
         * ------------
         * ~ Facultatif
         * ~ Texte simple
         */
        if (empty($_POST["mdp_connex"])) {
            $mdp_connex_err = TRUE;
        } else {
            $mdp_connex = test_input($_POST["mdp_connex"]);
        }

        /*
         * Mot de passe
         * ------------
         * ~ Facultatif
         * ~ Texte simple
         */
        if ($_POST["mdp_connex"] == ($_POST["mdpv_connex"])) {
            $mdpv_connex = test_input($_POST["mdpv_connex"]);
        } else {
            $mdpv_connex_err = TRUE;
        }

        /*
         * S'il n'y a pas d'erreur, ajout de l'utilisateur	 dans la base
         * Sinon, message d'erreur
         */
        if (!($nom_err && $prenom_err && $id_connex_err && $mdp_connex_err && $mdpv_connex_err)) {

            $mdp_connex = password_hash($mdp_connex, PASSWORD_DEFAULT);
            /*Cette fonctionnalité ne marche qu'a partir de la version 5.5 php*/

            $mdp_connex = $mdp_connex;

            $sql = "INSERT INTO `UTILISATEUR`(`NOM`, `PRENOM`, `NUM_CLASSE`, `FONCTION`,`TEL`, `MAIL`, `ID_CONNEX`, `PASS_CONNEX`, `MODIF_PASS`)\n"
                ."VALUES (\"$nom\", \"$prenom\", $classe, \"$fonction\", \"$tel\", \"$email\", \"$id_connex\", \"$mdp_connex\", \"1\");";

            try {
                $bdd->exec($sql);
            } catch (PDOException $pdoe) {
                $pdo_erreur = TRUE;
            }

            $snackbar = $pdo_erreur ? "Désolé mais une erreur est apparue, impossible d'ajouté l'utilisateur".'<br><span>'.$pdoe->getMessage().'</span>' : "Le utilisateur \"$nom $prenom\" a correctement été ajouté !";
            $nom = $prenom = $classe = $fonction = $tel = $email = $id_connex = $mdp_connex = $mdpv_connex = NULL;
            // header('location: '.$_SERVER['PHP_SELF']);

        } else {

            $snackbar = "Le utilisateur que vous avez rentré est incorrect.";

        }

    }

    /*
     * Supprimé un utilisateur
     */
    if (isset($_POST['sup_utilisateur'])) {

        $pdo_erreur = false;

        $agent = $_POST['agent'];

        $sql = "DELETE FROM `UTILISATEUR`\n"
            ."WHERE `ID` = $agent;";

        try {
            $bdd->exec($sql);
        } catch (PDOException $pdoe) {
            $pdo_erreur = TRUE;
        }

        if ($pdo_erreur) {
            if ($pdoe->getCode() == 23000)
                $msg = "<span>Vous ne pouvez pas supprimer cet utilisateur car il est utilisé dans un ou des ticket(s) et sa suppression entrainerait un malfonctionnement de l'application.</span>";
            else
                $msg = "Désolé mais une erreur est apparue, impossible de supprimé l'utilisateur".'<br>'.'<br><span>'.$pdoe->getMessage().'</span>';
        } else {
            $msg = "L'utilisateur a correctement été supprimé&nbsp;!";
        }

        $snackbar = $msg;

    }

    /*
     * Modifier un utilisateur
     */
    if (isset($_POST['mod_usr'])) {
        $pdo_erreur = false;

        $agent = $_POST['agent1'];
        $nom = $prenom = $type = $telephone = $mail = $identifiant = "";
        $fonction = "null";

        if (!empty($_POST['nom'])) {
            $nom = $_POST['nom'];
        }
        if (!empty($_POST['prenom'])) {
            $prenom = $_POST['prenom'];
        }
        if (!empty($_POST['classe'])) {
            $type = $_POST['classe'];
        }
        if (!empty($_POST['tel'])) {
            $telephone = $_POST['tel'];
        }
        if (!empty($_POST['email'])) {
            $mail = $_POST['email'];
        }
        if (!empty($_POST['fonction'])) {
            $fonction = $_POST['fonction'];
        }
        if (!empty($_POST['id_connex'])) {
            $identifiant = $_POST['id_connex'];
        }

        $sql = "UPDATE `UTILISATEUR`\n"
            ."SET NOM='$nom', PRENOM='$prenom',FONCTION='$fonction',MAIL='$mail',ID_CONNEX='$identifiant',TEL='$telephone',NUM_CLASSE=$type\n"
            ."WHERE `ID` = $agent;";
        try {
            $bdd->exec($sql);
        } catch (PDOException $pdoe) {
            $pdo_erreur = TRUE;
        }

        if ($pdo_erreur) {
            if ($pdoe->getCode() == 23000)
                $msg = "<span>Vous ne pouvez pas modifier cet utilisateur .</span>";
            else
                $msg = "Désolé mais une erreur est apparue, impossible de modifié l'utilisateur".'<br>'.'<br><span>'.$pdoe->getMessage().'</span>';
        } else {
            $msg = "L'utilisateur $nom $prenom a correctement été modifié&nbsp;!";
        }

        $snackbar = $msg;


    }
    /*
     * Modifier un mot de passe
     */
    if (isset($_POST['mod_pwd'])) {
        $mdpv_connex_err = $mdp_connex_err = false;
        $pdo_erreur = false;

        //recuperation de l'agent a qui on souhaite modifier le mot de passe
        $agent = $_POST['agent1'];

        //verification du mot de passe 1
        if (empty($_POST["mdp_connex"])) {
            $mdp_connex_err = TRUE;
        } else {
            $mdp_connex = test_input($_POST["mdp_connex"]);
        }

        //verification du mot de passe 2
        if ($_POST["mdp_connex"] == ($_POST["mdpv_connex"])) {
            $mdpv_connex = test_input($_POST["mdpv_connex"]);
        } else {
            $mdpv_connex_err = TRUE;
        }

        if (!($mdp_connex_err && $mdpv_connex_err)) {

            $mdp_connex = password_hash($mdp_connex, PASSWORD_DEFAULT);
            $mdp_connex = $mdp_connex;

            $sql = "UPDATE `UTILISATEUR`\n"
                ." SET PASS_CONNEX=\"$mdp_connex\""
                ." WHERE `ID` = $agent;";

            try {
                $bdd->exec($sql);
            } catch (PDOException $pdoe) {
                $pdo_erreur = TRUE;
            }

            $snackbar = $pdo_erreur ? "Désolé mais une erreur est apparue, impossible de modifier le mot de passe utilisateur".'<br><span>'.$pdoe->getMessage().'</span>' : "Le mot de passe a correctement été modifié !";
            $mdp_connex = $mdpv_connex = NULL;
            // header('location: '.$_SERVER['PHP_SELF']);

        } else {

            $snackbar = "Le mot de passe que vous avez rentré est incorrect.";

        }

    }

}

/**
 * Permet de créer une liste HTML (élément <select/>) contenant les colonnes de la table Lieu
 * @param  int $num Permet de déterminé l'option de la liste pré-selectionné
 * @param  PDO $pdo Instance de l'objet PDO permettant la connexion à la base de données
 * @return string Retourne les éléments HTML
 */
function listerClasse($num, PDO $pdo)
{

    $sql = "SELECT `NUM`, `NOM` FROM `CLASSE` ORDER BY `NOM` ASC;";
    $res = $pdo->query($sql);
    $table = $res->fetchAll(PDO::FETCH_ASSOC);

    $liste = '<select name="classe" required>'."\n";
    foreach ($table as $row) {

        $selected = $row['NUM'] == $num ? $selected = 'selected' : "";

        $liste .= '<option value="'.$row['NUM'].'" '.$selected.'>'
            .$row['NOM']
            .'</option>'."\n";

    }
    $liste .= '</select>';
    return $liste;
}

/**
 * Permet de créer une liste HTML (élément <select/>) contenant les colonnes nom/prénom de la table Utilisateurs
 * @param  int $num Permet de déterminé l'option de la liste pré-selectionné
 * @param  PDO $pdo Instance de l'objet PDO permettant la connexion à la base de données
 * @return string Retourne les éléments HTML
 */
function listerAgent($num, PDO $pdo)
{

    $sql = "SELECT `ID`, CONCAT(`NOM`, \" \", `PRENOM`) AS `NOM` FROM `UTILISATEUR` ORDER BY `NUM_CLASSE`, `NOM` ASC ;";
    $res = $pdo->query($sql);
    $table = $res->fetchAll(PDO::FETCH_ASSOC);

    $liste = '<select name="agent">'."\n";
    foreach ($table as $row) {

        $selected = "";

        if ($row['ID'] == $num)
            $selected = 'selected';

        $liste .= '<option value="'.$row['ID'].'" '.$selected.'>'
            .$row['NOM']
            .'</option>'."\n";
    }
    $liste .= '</select>';
    return $liste;
}


/*
 * Liste utilisateurs
 * ------------------
 */

function listerUsr(PDO $pdo)
{
    $sql = "SELECT `ID`, `CLASSE`.`NOM` AS `NOM_CLASSE`, CONCAT(`UTILISATEUR`.`NOM`, \" \", `UTILISATEUR`.`PRENOM`) AS `NOM_UTILISATEUR`, `TEL`, `MAIL`, `ID_CONNEX`\n"
        ."FROM `UTILISATEUR`\n"
        ."INNER JOIN `CLASSE`\n\t\t"
        ."ON `UTILISATEUR`.`NUM_CLASSE` = `CLASSE`.`NUM`\n"
        ."ORDER BY `UTILISATEUR`.`NUM_CLASSE`, `UTILISATEUR`.`NOM` ASC;";

    try {
        $res = $pdo->query($sql);
        $table = $res->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $pdoe) {
        $snackbar = "Impossible de récupérer l'utilisateur ... <br><span>".$pdoe->getMessage()."</span>";
        $table = array(array());
    }

    $liste_utilisateur = '<table>'
        .'<tr>'
        .'<th>Nom</th>'
        .'<th>Type</th>'
        .'<th>Téléphone</th>'
        // . '<th>Mail</th>'
        .'<th>Identifiant</th>'
        .'</tr>';

    $personnel = 0;

    foreach ($table as $row) {
        if ($row["NOM_CLASSE"] !== "Personnel") {
            $liste_utilisateur .= '<tr>'
                .'<td>'.$row['NOM_UTILISATEUR'].'</td>'
                .'<td>'.$row['NOM_CLASSE'].'</td>'
                .'<td>'.$row['TEL'].'</td>'
                // . '<td>'.$row['MAIL'].'</td>'
                .'<td>'.$row['ID_CONNEX'].'</td>'
                .'</tr>';
        } else {
            $personnel = $personnel + 1;
        }
    }

    $liste_utilisateur .= '</table><p>Il y a '.$personnel.' personnel present.</p>';

    return $liste_utilisateur;

}

//function listerModUsr($num,PDO $pdo) {

$sql = "SELECT `ID`, CONCAT(`NOM`, \" \", `PRENOM`) AS `NOM` FROM `UTILISATEUR` ORDER BY `NUM_CLASSE`, `NOM` ASC ;";
$res = $bdd->query($sql);
$table = $res->fetchAll(PDO::FETCH_ASSOC);
$num = "";

$liste = "<select name='agent1' id='agent1'>"."\n";
foreach ($table as $row) {

    $selected = "";

    if ($row['ID'] == $num)
        $selected = 'selected';

    $liste .= '<option value="'.$row['ID'].'" '.$selected.'>'
        .$row['NOM']
        .'</option>'."\n";
}
$liste .= '</select>'."\r";
//echo $liste;

$num = $table[0]['ID'];


$requete = "SELECT `ID`, `CLASSE`.`NOM` AS `NOM_CLASSE`, `NUM_CLASSE`, `UTILISATEUR`.`NOM`, `UTILISATEUR`.`PRENOM`, `TEL`, `MAIL`, `FONCTION`, `ID_CONNEX`\n"
    ."FROM `UTILISATEUR`"
    ."INNER JOIN `CLASSE`"
    ."ON `UTILISATEUR`.`NUM_CLASSE` = `CLASSE`.`NUM`"
    ."WHERE ID =$num";
// . "ORDER BY `UTILISATEUR`.`NUM_CLASSE`, `UTILISATEUR`.`NOM` ASC;";

$resultats = $bdd->query($requete);
$tabModifUser = $resultats->fetchALL(PDO::FETCH_ASSOC);
try {
    $resultats = $bdd->query($requete);

} catch (PDOException $e) {
    $message = "probleme pour acceder aux utilisateurs<br/>";
    $message = $message.$e->getMessage();
}


?>
<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <?php

        // Inclusion des éléments placé dans la balise <head>
        include_once("../struct/head.php");
        $titre_page = "Gestion des utilisateurs".$title;
        echo '<title>'.$titre_page.'</title>';

        ?>
        <link rel="stylesheet" type="text/css" href="../css/admin.css">
        <link rel="stylesheet" href="../css/creer_ticket.css">
    </head>
    <body>
        <div id="page">
            <?php

            // Inclusion de l'en-tête
            include_once "../struct/header.php";

            ?>

            <section>
                <h1>Gestion des utilisateurs</h1>
                <div class="container">
                    <div class="sep li">
                        <h2>Liste des utilisateurs</h2>
                        <?php echo listerUsr($bdd); ?>
                    </div>
                </div>

                <div class="container">
                    <div class="sep aj">
                        <h2>Ajouter un utilisateur</h2>
                        <br>
                        <h3>Par CSV</h3>

                        <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
                              enctype="multipart/form-data">

                            <div class="mi_largeur">
                                <input type="file" name="fileupload" value="fileupload" id="fileupload">
                            </div>

                            <div class="pleine_largeur boutons">
                                <input class="button" type="submit" name="nv_utilisateur_csv"
                                       value="Ajouter les utilisateur">
                            </div>
                        </form>
                    </div>

                    <div class="sep aj">
                        <br>
                        <h3>Manulement</h3>
                        <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <div class="mi_largeur">
                                <label for="nom">Nom *</label>
                                <input type="text" id="nom" name="nom" value="<?php echo $nom; ?>" required
                                       placeholder="Nom de l'utilisateur ...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="prenom">Prénom *</label>
                                <input type="text" id="prenom" name="prenom" value="<?php echo $prenom; ?>" required
                                       placeholder="Prénom de l'utilisateur ...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="classe">Type d'utilisateur *</label>
                                <?php echo listerClasse(0, $bdd); ?>
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="fonction">Fonction</label>
                                <input type="fonction" id="fonction" name="fonction" value="<?php echo $fonction; ?>"
                                       placeholder="Fonction de l'utilisateur ...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="tel">Numéro de téléphone</label>
                                <input type="tel" id="tel" name="tel" value="<?php echo $tel; ?>"
                                       placeholder="Numéro de téléphone ...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="email">Adresse email</label>
                                <input type="email" id="email" name="email" value="<?php echo $email; ?>"
                                       placeholder="Adresse email...">
                            </div><!--
					 -->
                            <hr><!--
					-->
                            <div class="pleine_largeur">
                                <label for="id_connex">Identifiant *</label>
                                <input type="text" id="id_connex" name="id_connex" value="<?php echo $id_connex; ?>"
                                       required placeholder="Identifiant de connexion...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="mdp_connex">Mot de passe *</label>
                                <input type="password" id="mdp_connex" name="mdp_connex"
                                       value="<?php echo $mdp_connex; ?>" required placeholder="Mot de passe ...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="mdpv_connex">Confirmer votre mot de passe *</label>
                                <input type="password" id="mdpv_connex" name="mdpv_connex"
                                       value="<?php echo $mdpv_connex; ?>" required
                                       placeholder="Mot de passe de vérification ...">
                            </div><!--
					-->
                            <div class="pleine_largeur boutons">
                                <input class="button" type="submit" name="nv_utilisateur" value="Ajouter l'utilisateur">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container">
                    <div class="sep sup">
                        <h2>Supprimer un utilisateur</h2>
                        <p>Supprimez un utilisateur en le selectionnant dans la liste ci-dessous.</p>
                        <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
                              onsubmit="return confirm(Êtes vous sûr de vouloir supprimer cet utilisateur ?);">
                            <div class="pleine_largeur">
                                <?php echo listerAgent($agent, $bdd); ?>
                            </div><!--
						-->
                            <div class="pleine_largeur boutons">
                                <input class="button" type="submit" name="sup_utilisateur"
                                       value="Supprimer l'utilisateur">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container">
                    <div class="sep mod">
                        <h2>Modifier l'utilisateur</h2>
                        <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                            <?php //echo listerModUsr($agent,$bdd);?>
                            <br>
                            <?= $liste ?>
                            <br>
                            <br>
                            <?php foreach ($tabModifUser as $row): ?>
                                <div id="formModif">
                                    <div class="mi_largeur">
                                        <label for="nom">Nom *</label>
                                        <input type="text" id="nom" name="nom" value="<?= $row['NOM'] ?>" required
                                               placeholder="Nom de l'utilisateur ...">
                                    </div><!--
					-->
                                    <div class="mi_largeur">
                                        <label for="prenom">Prénom *</label>
                                        <input type="text" id="prenom" name="prenom" value="<?= $row['PRENOM'] ?>"
                                               required placeholder="Prénom de l'utilisateur ...">
                                    </div><!--
					-->
                                    <div class="mi_largeur">
                                        <label for="classe">Type d'utilisateur *</label>
                                        <?= listerClasse($row['NUM_CLASSE'], $bdd); ?>
                                    </div><!--
					-->
                                    <div class="mi_largeur">
                                        <label for="fonction">Fonction</label>
                                        <input type="fonction" id="fonction" name="fonction"
                                               value="<?= $row['FONCTION'] ?>"
                                               placeholder="Fonction de l'utilisateur ...">
                                    </div><!--
					-->
                                    <div class="mi_largeur">
                                        <label for="tel">Numéro de téléphone</label>
                                        <input type="tel" id="tel" name="tel" value="<?= $row['TEL'] ?>"
                                               placeholder="Numéro de téléphone ...">
                                    </div><!--
					-->
                                    <div class="mi_largeur">
                                        <label for="email">Adresse email</label>
                                        <input type="email" id="email" name="email" value="<?= $row['MAIL'] ?>"
                                               placeholder="Adresse email...">
                                    </div><!--
					 -->
                                    <hr><!--
					-->
                                    <div class="pleine_largeur">
                                        <label for="id_connex">Identifiant *</label>
                                        <input type="text" id="id_connex" name="id_connex"
                                               value="<?= $row['ID_CONNEX'] ?>" required
                                               placeholder="Identifiant de connexion...">
                                    </div><!--
					-->
                                    <div class="pleine_largeur boutons">
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="boutons">
                                <input class="button" type="submit" name="mod_usr" value="Modifier la/les catégorie(s)">
                                <input class="button" type="reset" value="Annuler les modifications">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="container">
                    <div class="sep sup">
                        <h2>Enregistrer un nouveau mot de passe</h2>
                        <p>Modifier le mot de passe pour un utilisateur.</p>
                        <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
                              onsubmit="return confirm('Êtes vous sûr de vouloir modifier le mot de passe ?');">
                            <?php echo $liste; ?>
                            <br/>
                            <br/>
                            <!--
                        -->
                            <div class="mi_largeur">
                                <label for="mdp_connex">Mot de passe *</label>
                                <input type="password" id="mdp_connex" name="mdp_connex"
                                       value="<?php echo $mdp_connex; ?>" required placeholder="Mot de passe ...">
                            </div><!--
					-->
                            <div class="mi_largeur">
                                <label for="mdpv_connex">Confirmer votre mot de passe *</label>
                                <input type="password" id="mdpv_connex" name="mdpv_connex"
                                       value="<?php echo $mdpv_connex; ?>" required
                                       placeholder="Mot de passe de vérification ..."><br/>
                                <div class="boutons">
                                    <input class="button" type="submit" name="mod_pwd"
                                           value="Modifier le mots de passe">
                                    <input class="button" type="reset" value="Annuler les modifications">
                                </div>
                        </form>
                    </div>
                </div>
        </div>
        </section>

        <?php

        // Inclusion du pied de page (footer)
        include_once "../struct/footer.php";

        ?>

        </div>
        <script>
            var listeagent = document.getElementById('agent1');
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                var listeinfo = document.getElementById('formModif');
                if (xhr.readyState == 4 && xhr.status == 200) {
                    listeinfo.innerHTML = xhr.responseText;
                }
            }
            listeagent.onchange = function () {
                var agent1 = listeagent.value;
                xhr.open('POST', '../Ajax/ajax.php', null);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.send('agent1=' + agent1);
            }
        </script>
    </body>
</html>
