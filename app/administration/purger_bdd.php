<?php
/**
 * Purger la base de données
 *
 * @author Emilie Graton (V. 1 - 2012)
 * @author Anthony Lozano (2015)
 *
 * @version  2.0.0
 */



/*
 * Inclusion(s)
 * ------------
 * ~ Session
 * ~ Connexion à la base
 * ~ Module de tri
 */
include_once "../struct/session.php";
include_once "../php/t_connex_bd.php";
include_once "../php/classer_tickets.php";
include_once "../php/snackbar.php";


$_SESSION['action'] = 'purge';
$action = $_SESSION['action'];

// $get['regroup'] = isset($_GET['regroup'])?$_GET['regroup']:'d_ouverture';
$order_by = array('D_OUVERTURE', 'NUM_IMPORTANCE');
$ordre = "ASC";


//Si l'utilisateur a envoyer le formulaire
if (isset($_POST['previsualiser'])) {
	
	// Initialisation des variables d'erreur	
	$chiffre_err = $date_err = FALSE;

	/*
	Verification que l'element envoyer est bien numérique
	*/
	if (!is_int(intval($_POST['duree1']))){
		$chiffre_err = TRUE;
	} 

	/*
	Verification que l'element envoyer est bien en mois ou en année
	*/

	if ($_POST['duree2'] != 'month' || $_POST['duree2'] != 'year'){
		$date_err = TRUE;
	} 

	/*
	 * Si le chiffre & l'année ou le mois sont correct, on execute
	 */
	if (!($chiffre_err && $date_err)) {
		/*
		* Si il s'agit d'une année selectionner on entre dans le if
		* Sinon on execute le else pour les mois
		*/
		if ($_POST['duree2'] == "year") {
			$nbMois = $_POST['duree1']*12;
		} else {
			$nbMois = $_POST['duree1']; 		
		}
		//soustraction du nombre de mois en fonction de la date d'aujourd'hui
		$timestamp = strtotime("-$nbMois month");	
		$date = date("Y-m-d", $timestamp);
		$liste = grouperTicket('D_OUVERTURE', $order_by, $ordre, $action, $bdd, $date);

	}	
} 
//traitement effectuer suite a l'appuie de bouton envoyer
elseif((isset($_POST['Supprimer']))){
	if ($_POST['duree2'] == "year") {
		$nbMois = $_POST['duree1']*12;			
	} else {
		$nbMois = $_POST['duree1']; 		
	}
		//soustraction du nombre de mois en fonction de la date d'aujourd'hui
	$timestamp = strtotime("-$nbMois month");	
	$date = date("Y-m-d", $timestamp);
	var_dump($date);
	$res = $bdd->beginTransaction();
	$res = $bdd->query("SELECT DISTINCT `D_OUVERTURE`, ID, ID_RESPONSABLE, NUM_CATEGORIE, NUM_LIEU, NUM_STATUT,ID_CREATEUR, NUM_IMPORTANCE, TITRE,DESCRIPTION, H_OUVERTURE, D_CLOTURE FROM `TICKET` WHERE `TICKET`.`D_CLOTURE` <= '$date' AND `TICKET`.`D_CLOTURE` <> '0000-00-00' AND `TICKET`.`NUM_STATUT` = 5 ORDER BY `TICKET`.`D_OUVERTURE` $ordre;");
	$table = $res->fetchAll(PDO::FETCH_ASSOC);
	foreach($table as $ligne){
		$id = $id_responsable = $num_categorie = $num_lieu = $num_statut = $id_createur = $titre = $description = $d_ouverture = $h_ouverture = $d_cloture = " ";
		$id=$ligne['ID'];
		$titre =$ligne['TITRE'];
		$description = $ligne['DESCRIPTION'];
		$d_ouverture = $ligne['D_OUVERTURE'];
		$h_ouverture = $ligne['H_OUVERTURE'];
		$d_cloture = $ligne['D_CLOTURE'];
		
		//selection de la categorie, du nom, du lieu, du statut et de la classe du createur pour le ticket
		$res= $bdd->query("SELECT `CATEGORIE`.NOM as cat, `UTILISATEUR`.NOM,`LIEU`.NOM as lieu,`STATUT`.NOM as stat,`CLASSE`.NOM as classe FROM TICKET INNER JOIN UTILISATEUR ON UTILISATEUR.ID=TICKET.ID_RESPONSABLE INNER JOIN CATEGORIE ON CATEGORIE.NUM=TICKET.NUM_CATEGORIE INNER JOIN LIEU ON LIEU.NUM=TICKET.NUM_LIEU INNER JOIN STATUT ON STATUT.NUM=TICKET.NUM_STATUT INNER JOIN CLASSE ON CLASSE.NUM=TICKET.ID_CREATEUR WHERE TICKET.`ID`=$id;");
		$tab = $res->fetchAll(PDO::FETCH_ASSOC);
		//instanciation des variables
		foreach($tab as $tab){
			$num_categorie = $tab['cat'];
			$id_createur =$tab['classe'];
			$id_responsable = $tab['NOM'];
			$num_lieu =$tab['lieu'];
			$num_statut =$tab['stat'];
		}

		//creation du ticket dans la table historique
		$sql= "INSERT INTO `HISTORIQUE`(`ID`, `RESPONSABLE`, `CATEGORIE`, `LIEU`, `STATUT`, `CREATEUR`, `TITRE`, `DESCRIPTION`, `D_OUVERTURE`, `H_OUVERTURE`, `D_CLOTURE`) VALUES ($id,'$id_responsable','$num_categorie','$num_lieu','$num_statut','$id_createur', '$titre', '$description','$d_ouverture','$h_ouverture','$d_cloture'); ";
		$res= $bdd->query($sql);
		
		//suppresion de la table evolution
		$res= $bdd->query("DELETE FROM `EVOLUTION` WHERE `ID_TICKET`=$id;");
		
		//suppresion de la table ticket
		$res= $bdd->query("DELETE FROM `TICKET` WHERE `ID`=$id;");

	} 
	//envoie de la requete de transaction
	$res=$bdd->commit();
	$liste = "<div>Il n'y as plus de  ticket à supprimer</div>";
} else {
	$liste = '<div>Aucun paramètre selectionné</div>';
}

?>
<!DOCTYPE html>
<html lang="fr-FR">
<head>
	<?php

	// Inclusion des éléments placé dans la balise <head>
	include_once("../struct/head.php");
	$titre_page = "Purger la base de données". $title;
	echo '<title>'.$titre_page.'</title>';

	?>
	<link rel="stylesheet" href="../css/lister_ticket.css">
	<link rel="stylesheet" href="../css/admin.css">
</head>
<body>
	<div id="page">
		<?php

		// Inclusion de l'en-tête
		include_once "../struct/header.php";

		?>

		<section>
			<h1>Purger les données de la base</h1>
			<p>Cette page supprime les tickets placés dans l'historique qui ont été validé depuis <b>la date de votre choix</b>.</p>
			<div class="container">
				<div class="sep li">
					<h2>Purger les tickets</h2>
					<form method="POST">
						<p>
							Sélectionner les tickets validés depuis plus de
							<input type="number" min="1" max="12" name="duree1" id="duree1" value="<?= isset($_POST["duree1"]) ? $_POST["duree1"] : "2" ?>">
							<select name="duree2" id="duree2">
								<?php $value = isset($_POST["duree2"]) ?  $_POST["duree2"] : "month" ?>
								<option value="month" <?= $value=="month" ? "selected": "" ?> >mois</option>
								<option value="year" <?= $value=="year" ? "selected": "" ?> >ans</option>
							</select>
						</p>
						<input class="button" type="submit" name="previsualiser" value="Aperçu">
<!-- 						<input class="button" type="submit" name="Supprimer" value="Supprimer">
 -->					</form>
				</div>		
				<?php echo $liste; ?>
			</div>
		</section>

		<?php

		// Inclusion du pied de page (footer)
		include_once "../struct/footer.php";

		?>

	</div>
</body>
</html>
