initTable();
initDescription();

// Met en place la possibilité de trier sur chaque tableau de classe "avectri"
function initTable(){
	[].forEach.call( document.getElementsByClassName("avectri"), function(tableObject) {
		// On récupère l'entête du tableau
		var theadObject = tableObject.getElementsByTagName("thead")[0];
		var thIndex = 1;
		// On boucle sur les enfants "th" de cette entête
		// On leur ajoute une ecoute du click
		[].forEach.call( theadObject.querySelectorAll("th"), function(thObject) {
			// Si la colonne n'est pas le détail ou l'imprimante 	
			if(!(thObject.className === "detail" || thObject.className === "icoprint")){
				thObject.addEventListener("click", triTable, false);
				thObject.setAttribute("data-pos", thIndex);
				// Évaluation et assignation de la nouvelle direction
				thObject.setAttribute("data-tri", "0");
				thIndex ++;
			}
		});
	});
}

function triTable(){
	var indexTri = this.getAttribute("data-tri");
	this.setAttribute("data-tri", (indexTri == "0") ? "1" : "0");

	// On construit une matrice qui comprend le contenu des cellules
	// Récupère le tableau (tbody)
	var tbodyObject = this.parentNode.parentNode.parentNode.getElementsByTagName("tbody")[0];
	
	// On recup toutes les lignes du tbody
	var rowObject = tbodyObject.rows;
	// On recup le nombre de ligne du tbody
	var nbRow = rowObject.length;
	var arrayColumn = new Array(),
		cellsObject;
	for(var i = 0; i < nbRow; i = i + 2) {
		var indexI = i/2;
		cellsObject = rowObject[i].cells;
		arrayColumn[indexI] = new Array();
		for(var j = 0; j < cellsObject.length; j++){
			var indexJ = j;
			arrayColumn[indexI][indexJ] = cellsObject[indexJ].innerHTML;
		}
		// On ajoute à la suite la ligne contenant la description
		arrayColumn[indexI].push(rowObject[i+1]);
	}

	// Trier la matrice (array)
	// Récupère le numéro de la colonne
	var nIndex = this.getAttribute("data-pos");
	// Récupère le type de tri (numérique ou par défaut « local »)
	var sFonctionTri = (this.getAttribute("data-type")=="num") ? "compareNombres" : "compareLocale";
	// Tri
	arrayColumn.sort(eval(sFonctionTri));
	// Tri numérique
	function compareNombres(a, b) {return a[nIndex-1] - b[nIndex-1];}
	// Tri local (pour support utf-8)
	function compareLocale(a, b) {return a[nIndex-1].localeCompare(b[nIndex-1]);}
	// Renverse la matrice dans le cas d’un tri descendant
	if (indexTri==0) arrayColumn.reverse();

	// On vide le contenu du tbody
	var fc = tbodyObject.firstChild;
	while( fc ) {
		tbodyObject.removeChild( fc );
		fc = tbodyObject.firstChild;
	}
	// Construit les colonnes du nouveau tableau
	for(i = 0; i < nbRow/2; i++){
		// On récupère la ligne de description
		var descriptionRow = arrayColumn[i].pop();
		// On concatene toutes les cellules du ticket
		arrayColumn[i] = "<td>"+arrayColumn[i].join("</td><td>")+"</td>";
		// On créé un ligne de tableau contennt les cellules
		var node = document.createElement("tr");
		node.addEventListener("click", displayDescription, false);
		node.innerHTML = arrayColumn[i];
		// On ajoute cette ligne au tbody
		tbodyObject.appendChild(node);
		// On ajoute à la suite la description qui correspond
		tbodyObject.appendChild(descriptionRow);
	}

}

// Cache ou affiche la description
function displayDescription(){
	var description = this.nextElementSibling;
	var display = description.style.display;
	if (display == null || display === "table-row"){
		description.style.display = "none";
	} else {
		description.style.display = "table-row";
	}
}

// 
function initDescription(){
	var elements = document.getElementsByClassName('description');
	console.log(elements);
	for(var i = 0; i < elements.length ; i++){
		elements[i].previousElementSibling.addEventListener("click", displayDescription, false);
		elements[i].style.display = "none";
	}

}