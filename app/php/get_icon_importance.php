<?php
/**
 * Description
 * @param type $importance 
 * @return type
 */
function getIconImportance($importance) {

    $base = explode('/', (pathinfo($_SERVER["REQUEST_URI"])["dirname"]));

    $urlDir = ['', 'ticket', 'administration', 'Ajax', "bd", "css", "doc", "historique", "image_ticket", "images", "js", "php", "struct", "ticket"];

    if (count($base) > 1) {
        if (count($base) == 2 && in_array($base[1], $urlDir)) {
            $base = "";
        } elseif ($base[1] !== '') {
            $base = "/".$base[1];
        } elseif (count($base) > 2) {
            for ($i = 1; $i <= count($base) + 1; $i++) {
                if (isset($base[$i + 1]) && isset($base[$i])) {
                    if ((in_array($base[$i + 1], $urlDir))) {
                        $real_base = "/";

                        for ($a = 1; $a <= $i; $a++) {
                            if (isset($base[$a])) {
                                $real_base = $real_base.$base[$a]."/";
                            }
                        }
                    }
                }
            }
        }
        $base = isset($real_base) == false ? $base : $real_base;
    }

    $importance = intval($importance);

	switch ($importance) {

		case 1:
			$nom_icone = "basse";
			break;

		case 2:
			$nom_icone = "moyenne";
			break;

		case 3:
			$nom_icone = "haute";
			break;

		case 4:
			$nom_icone = "urgente";
			break;

		default:
			$nom_icone = "nulle";
			break;
	}

    $icone = '<img src="'.$base.'/images/iconnav/'.$nom_icone.'.png" class="icon_imp">';

	return $icone;
}

?>