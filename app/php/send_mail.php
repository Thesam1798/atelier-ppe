<?php
/**
 * Created by IntelliJ IDEA.
 * User: alexandre_debris
 * Date: 14/09/18
 * Time: 16:45
 *
 * @param String $formUser , Default Null, Nom et prenom de l'utilisateur qui envoie l'email
 *
 * @param String $formEmail , Default Null, Email de l'utilisateur qui envoie l'email
 *
 * @param String $toEmail , Default Null,  Email de l'utilisateur qui recoie
 *
 * @param String $toName , Default Null, Nom et prenom de l'utilisateur qui recoie
 *
 * @param String $texte , Default Null, Texte de l'email
 *
 * @param String $newticket , Default False, True si c'est la creation d'un ticket
 *
 * @return string
 */
require 'mailer/Exception.php';
require 'mailer/PHPMailer.php';
require 'mailer/SMTP.php';

function sendMail($formUser, $formEmail, $toEmail, $toName, $texte, $typeTexte = 0)
{

    include_once dirname(dirname(__FILE__)).'/php/var_mail.php';

    $var_mail = new var_mail();

    if (is_null($formEmail) || $formEmail == "" || strpos($formEmail, '@') == false) {
        return "Adresse e-mail de l'expediteur invalide.";
    } elseif (is_null($toEmail) || $toEmail == "" || strpos($toEmail, '@') == false) {
        return "Adresse e-mail du destinataire invalide.";
    }

    $mail = new \PHPMailer\PHPMailer\PHPMailer(true);
    $mail->setLanguage('fr', 'mailer/language/');

    try {
        //Server settings
        $mail->SMTPDebug = 0;                                   // Enable verbose debug output
        $mail->isSMTP();                                        // Set mailer to use SMTP
        $mail->Host = $var_mail->VAR_MAIL_SMTP;                         // Specify main and backup SMTP servers //smtp.gmail.com
        $mail->SMTPAuth = true;                                 // Enable SMTP authentication
        $mail->Username = $var_mail->VAR_MAIL_Username;             // SMTP username
        $mail->Password = $var_mail->VAR_MAIL_Password;                         // SMTP password //affulymendncuymv
        $mail->SMTPSecure = $var_mail->VAR_MAIL_Methode;                              // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $var_mail->VAR_MAIL_Port;                                      // TCP port to connect to
        $mail->Timeout = 20;

        //Recipients
        $mail->addAddress($toEmail, $formUser);
        $mail->addReplyTo($formEmail, $formUser);
        $mail->setFrom($formEmail, 'Commentaire de la part de : '.$formUser);

        // TODO Coriger les texte

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        if ($typeTexte == 1) {
            $mail->Subject = 'Atelier : Un nouveaux ticket de '.$formUser;
            $mail->Body = 'Un nouveau ticket de '.$formUser.' sur l\'application Atelier :<br><br>'.$texte.'<br><hr><br>Vous pouvez vous connecter a l\'application a partir de l\'URL suivant : <a href="http://'.$_SERVER['HTTP_HOST'].'/">http://'.$_SERVER['HTTP_HOST'].'/</a>';
            $mail->AltBody = 'Un nouveaux ticket sur l\'application Atelier :     '.$texte.'     Vous pouver vous connecter a l\'application a partire de l\'url suivant : http://'.$_SERVER['HTTP_HOST'].'/';

        } elseif ($typeTexte == 2) {
            $mail->Subject = 'Atelier : Votre ticket a ete valider';
            $mail->Body = 'Votre ticket : '.$texte.' sur l\'application Atelier a ete valider.<br><hr><br>Vous pouvez vous connecter a l\'application a partir de l\'URL suivant : <a href="http://'.$_SERVER['HTTP_HOST'].'/">http://'.$_SERVER['HTTP_HOST'].'/</a>';
            $mail->AltBody = 'Votre ticket : '.$texte.' sur l\'application Atelier a ete valider. Vous pouvez vous connecter a l\'application a partir de l\'URL suivant : http://'.$_SERVER['HTTP_HOST'].'/';

        } elseif ($typeTexte == 3) {
            $mail->Subject = 'Atelier : Votre ticket a ete refuser';
            $mail->Body = 'Votre ticket a ete refuse sur l\'application Atelier pour le motif suivant : '.$texte.' <br><hr><br>Vous pouvez vous connecter a l\'application a partir de l\'URL suivant : <a href="http://'.$_SERVER['HTTP_HOST'].'/">http://'.$_SERVER['HTTP_HOST'].'/</a>';
            $mail->AltBody = 'Votre ticket a ete refuse sur l\'application Atelier pour le motif suivant : '.$texte.' Vous pouvez vous connecter a l\'application a partir de l\'URL suivant : http://'.$_SERVER['HTTP_HOST'].'/';

        } else {
            $mail->Subject = 'Atelier : Un nouveaux commentaire';
            $mail->Body = 'Un nouveau commentaire de '.$formUser.' sur l\'application Atelier :<br><br>'.$texte.'<br><hr><br>Vous pouvez vous connecter a l\'application a partir de l\'URL suivant : <a href="http://'.$_SERVER['HTTP_HOST'].'/">http://'.$_SERVER['HTTP_HOST'].'/</a>';
            $mail->AltBody = 'Un nouveaux commentaire sur l\'application Atelier :     '.$texte.'     Vous pouver vous connecter a l\'application a partire de l\'url suivant : http://'.$_SERVER['HTTP_HOST'].'/';
        }

        $mail->send();
    } catch (Exception $e) {
        return "Imposible d'envoyer l'email : ".$mail->ErrorInfo;
    }
    return "";
}

/**
 * Permet de recuperer l'utilisateur en fonction de sont ID
 * @param  int $id_usr Identifiant de l'auteur du commentaire
 * @param  PDO $pdo Instance de l'objet PDO permettant de dialoguer avec la base de donnee
 * @return array
 */
function getUser($id_usr, PDO $pdo)
{

    // Insertion de l'evolution
    // -----------------------

    $sth = $pdo->prepare("SELECT * FROM UTILISATEUR WHERE ID = ?");

    try {
        $sth->execute([$id_usr]);
        $pdo_erreur = FALSE;
    } catch (PDOException $pdoe) {
        $pdo_erreur = TRUE;
    }

    if ($pdo_erreur) {
        $_SESSION['msg'] = "Erreur interne&nbsp;:<br>".'<span>'.$pdoe->getMessage().'</span>';
    }

    return $pdo_erreur ? [] : $sth->fetchAll();

}

/**
 * Permet de recuperer l'utilisateur qui a creer le ticket en fonction de ID du ticket
 * @param  $id_ticket $id_usr Identifiant de l'auteur du commentaire
 * @param  PDO $bdd Instance de l'objet PDO permettant de dialoguer avec la base de donnee
 * @return array
 */

function getUserFromPost($id_ticket, $bdd)
{

    $sth = $bdd->prepare("SELECT * FROM TICKET WHERE ID = ?");

    try {
        $sth->execute([$id_ticket]);
        $pdo_erreur = FALSE;
    } catch (PDOException $pdoe) {
        $pdo_erreur = TRUE;
    }

    if ($pdo_erreur) {
        $_SESSION['msg'] = "Erreur interne&nbsp;:<br>".'<span>'.$pdoe->getMessage().'</span>';
    } else {
        $temp = ($sth->fetchAll());
        return (getUser(($temp[0]['ID_CREATEUR']), $bdd));
    }

    return [];

}