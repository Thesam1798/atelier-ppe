<?php
/**
 * Page permettant d'afficher un message d'information "snack bar"
 *
 * @author Anthony Lozano (2015)
 *
 * @version  0.1.0
 */

/**
 * Permet d'afficher une snackbar comportand un message
 * @param  string $message Message à placer dans la snackbar
 * @return string Renvoie les éléments HTML permettant d'afficher la snackbar	
 */

function snackBar($message){

    $base = explode('/', (pathinfo($_SERVER["REQUEST_URI"])["dirname"]));

    $urlDir = ['', 'ticket', 'administration', 'Ajax', "bd", "css", "doc", "historique", "image_ticket", "images", "js", "php", "struct", "ticket"];

    if (count($base) > 1) {
        if (count($base) == 2 && in_array($base[1], $urlDir)) {
            $base = "";
        } elseif ($base[1] !== '') {
            $base = "/".$base[1];
        } elseif (count($base) > 2) {
            for ($i = 1; $i <= count($base) + 1; $i++) {
                if (isset($base[$i + 1]) && isset($base[$i])) {
                    if ((in_array($base[$i + 1], $urlDir))) {
                        $real_base = "/";

                        for ($a = 1; $a <= $i; $a++) {
                            if (isset($base[$a])) {
                                $real_base = $real_base.$base[$a]."/";
                            }
                        }
                    }
                }
            }
        }
        $base = isset($real_base) == false ? $base : $real_base;
    }

    $snackbar = '<div id="snackbar">'."\n\t"
			  . '<div id="snack">'."\n\t\t"
			  . '<p>'." Message d'information".'</p>'."\n\t"
			  . "</div>"."\n\t"
			  . '<div id="bar">'."\n\t\t"
			  . '<p>'.$message.'</p>'."\n\t\t"
			  . "</div>"."\n\t"
			  . "</div>"."\n";

	$snackbar .= '<script type="text/javascript" src="'.$base.'/js/snackbar.js"></script>';
	return $snackbar;
}

?>