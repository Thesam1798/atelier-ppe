<?php
/**
 * Gestion des utilisateurs
 *
 * @author Timothée Lagleyre (2015)
 *
 * @version  1.0
 */

/*
 * Inclusion(s)
 * ------------
 * ~ Session
 * ~ Connexion à la base
 */
include_once "../struct/session.php";
include_once "../php/t_connex_bd.php";

$num = $_POST['agent1'];

//charger les infos avec le premiers utilisateur
$requete="SELECT `ID`, `CLASSE`.`NOM` AS `NOM_CLASSE`, `NUM_CLASSE`, `UTILISATEUR`.`NOM`, `UTILISATEUR`.`PRENOM`, `TEL`, `MAIL`, `FONCTION`, `ID_CONNEX`\n"
	 . "FROM `UTILISATEUR`"
	 . "INNER JOIN `CLASSE`"
	 . "ON `UTILISATEUR`.`NUM_CLASSE` = `CLASSE`.`NUM`"
	 . "WHERE ID = $num"
	 . " LIMIT 1";
	// . "ORDER BY `UTILISATEUR`.`NUM_CLASSE`, `UTILISATEUR`.`NOM` ASC;";

$resultats=$bdd->query($requete);
$tab=$resultats->fetchALL(PDO::FETCH_ASSOC);
$row = $tab[0];
try{
	$resultats=$bdd->query($requete);

}
catch(PDOException $e){
	$message="probleme pour acceder aux utilisateurs<br/>";
	$message=$message.$e->getMessage();
}

?>
		<div class="mi_largeur">
			<label for="nom">Nom *</label>
			<input type="text" id="nom" name="nom" value="<?= $row['NOM'] ?>" required placeholder="Nom de l'utilisateur ...">
		</div><!--
		--><div class="mi_largeur">
			<label for="prenom">Prénom *</label>
			<input type="text" id="prenom" name="prenom" value="<?= $row['PRENOM'] ?>" required placeholder="Prénom de l'utilisateur ...">
			</div><!--
		--><div class="mi_largeur">
			<label for="classe">Type d'utilisateur *</label>
			<?= listerClasse($row['NUM_CLASSE'], $bdd) ?>
			</div><!--
		--><div class="mi_largeur">
			<label for="fonction">Fonction</label>
			<input type="fonction" id="fonction" name="fonction" value="<?= $row['FONCTION'] ?>" placeholder="Fonction de l'utilisateur ...">
			</div><!--
		--><div class="mi_largeur">
			<label for="tel">Numéro de téléphone</label>
			<input type="tel" id="tel" name="tel" value="<?= $row['TEL'] ?>" placeholder="Numéro de téléphone ...">
			</div><!--
		--><div class="mi_largeur">
			<label for="email">Adresse email</label>
			<input type="email" id="email" name="email" value="<?= $row['MAIL'] ?>" placeholder="Adresse email...">
			</div><!--
		 --><hr><!--
		--><div class="pleine_largeur">
			<label for="id_connex">Identifiant *</label>
			<input type="text" id="id_connex" name="id_connex" value="<?= $row['ID_CONNEX'] ?>" required placeholder="Identifiant de connexion...">
			</div><!--
		--><div class="pleine_largeur boutons">
			</div>
		</form>
<?php

	function listerClasse($num, PDO $pdo) {

		$sql = "SELECT `NUM`, `NOM` FROM `CLASSE` ORDER BY `NOM` ASC;";
		$res = $pdo->query($sql);
		$table = $res->fetchAll(PDO::FETCH_ASSOC);

		$liste = '<select name="classe" required>'."\n";
		foreach ($table as $row){

			$selected = $row['NUM'] == $num ? $selected = 'selected' : "";

			$liste .= '<option value="'.$row['NUM'].'" '.$selected.'>'
					. $row['NOM']
					. '</option>'."\n";
			
		}
		$liste .= '</select>';
		return $liste;
	}
