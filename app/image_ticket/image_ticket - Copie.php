<?php

/**
 * Cette classe permet de manipuler une image par ticket.
 * On enregistre l'image uploadé par l'utilisateur mais on manipule les images
 * selon les formats entrés en attributs.
 */
class ImageTicket {

	/**
	 * Contient les tailles des images utilisées dans l'appli
	 */
	private $formats = [
		'thumb' => [360, 200],
		'large' => [940, 530]
	];

	/**
	 * id du ticket lié à cette image
	 * @var int
	 */
	private $idTicket;

	/**
	 * extension du fichier
	 * @var string
	 */
	private $extension;


	/**
	 * Constructeur / Doit récupérer l'id du ticket
	 * @param int $idTicket Id du ticket lié à cette image
	 */
	public function __construct($idTicket){
		$this->idTicket = $idTicket;

	}

	/**
	 * Permet de supprimer l'image d'un ticket, et tous les formats associés
	 */
	public function suppr(){
        foreach($this->formats as $format => $dimensions){
            unlink($this->getImageDir() . $this->image($format));
        }
        unlink($this->getImageDir() . "/{$this->idTicket}.{$this->image}");
	}

	/**
	 * Retourne le dossier où est enregistré l'image
	 * @return string chemin du dossier des images du ticket
	 */
	private function getImageDir(){
		return '../images/tickets/'.ceil($this->idTicket / 1000);
	}

	/**
	 * Retourne les valeurs height/width du format demandé
	 * @param string $format nom du format souhaité
	 * @return array 1: valeur height / 2: valeur width
	 */
	public function getDimensions($format){
		foreach ($this->formats as $key => $value) {
			if($format == $key){
				return $value;
			}
		}
		return false;
	}

	/**
	 * Retourne l'image au format demandé
	 * @param  string $size nom du format de l'image demandé, selon ceux entrés en attribut
	 * @return string       path du fichier
	 */
	public function get($size){
		return '/'.$this->getImageDir().'/'.$this->idTicket.'_'.$size.'.jpg';
	}

	/**    
	 * Permet de créer et enregistrer la photo d'un ticket avec tous les formats
	 * @param $_FILES['photo'] $image Demande l'upload du fichier tel quel
	 */
	public function make($image){
		$isValid = true;

		if($isValid){
			if(!empty($extension)){
				unlink($this->getImageDir()."/{$this->idTicket}.$this->extension");
			}
			self::saved(function($instance) use ($image){
				$image = $image->move($instance->getImageDir(), $instance->id.'.'.$image->getClientOriginalExtension());
				foreach ($this->formats as $format => $dimensions) {
					ImageManagerStatic::make($image)->fit($dimensions[0], $dimensions[1])->save($this->getImageDir().$instance->image($format));
				}
			});

            //$this->attributes['image'] = $image->getClientOriginalExtension();
		}
	}

}