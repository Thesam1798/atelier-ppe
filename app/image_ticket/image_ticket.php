<?php
/**
 * Script de vérification, insertion, récupération d'image d'un ticket
 * D'après un script réalisé par Emacs - http://www.apprendre-php.com
 *
 */


/************************************************************
 * Definition des constantes / tableaux et variables
 *************************************************************/
 
// Constantes
define('TARGET', '../images/tickets');    // Repertoire cible
define('MAX_SIZE', 200000);    // Taille max en octets du fichier
define('WIDTH_MAX', 1000);    // Largeur max de l'image en pixels
define('HEIGHT_MAX', 1000);    // Hauteur max de l'image en pixels
 
// Tableaux de donnees
$tabExt = array('jpg','png','jpeg');    // Extensions autorisees
$infosImg = array();
$formats = [
	'thumb' => [360, 200],
	'large' => [940, 530]
];
 
// Variables
$idTicket='';
$extension = '';
$message = '';
$nomImage = '';

/************************************************************\
 * Méthodes publiques
\************************************************************/

/**
 * Vérifie les caractéristiques d'un fichier upload
 * @param  $_FILES['photo'] $file Envoyer directement le contenu de $FILES['fichier']
 * @return boolean       Image valide ou non
 */
function verifUpload($file){
	$ret = true;
	// On verifie si le parametre n'est pas vide
	if(empty($file)){
		$message = "Paramètre vide";
		$ret = false;
	} else {
		// Sinon le fichier est Ok, on le vérifie

		// On verifie si le champ est rempli
		if( !empty($file['name']) ) {
			// Recuperation de l'extension du fichier
			$extension  = pathinfo($file['name'], PATHINFO_EXTENSION);
	 
			// On verifie l'extension du fichier
			if(in_array(strtolower($extension),$tabExt)) {
				// On recupere les dimensions du fichier
				$infosImg = getimagesize($file['tmp_name']);

				// On verifie le type de l'image
				if($infosImg[2] >= 1 && $infosImg[2] <= 14) {
					// On verifie les dimensions et taille de l'image
					if(($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($file['tmp_name']) <= MAX_SIZE)) {
						// Parcours du tableau d'erreurs
						if(isset($file['error']) && UPLOAD_ERR_OK === $file['error']) {
							$ret = true;
						} else {
							$message = 'Une erreur interne a empêché l\'uplaod de l\'image';
							$ret = false;
						}
					} else {
						// Sinon erreur sur les dimensions et taille de l'image
						$message = 'Erreur dans les dimensions de l\'image !';
						$ret = false;
					}
				} else {
					// Sinon erreur sur le type de l'image
					$message = 'Le fichier à uploader n\'est pas une image !';
					$ret = false;
				}
			} else {
				// Sinon on affiche une erreur pour l'extension
				$message = 'L\'extension du fichier est incorrecte !';
				$ret = false;
			}
		} else {
			// Sinon on affiche une erreur pour le champ vide
			$message = 'Veuillez remplir le formulaire svp !';
			$ret = false;
		}
	}
	// var_dump($message);
	return $ret;
}


/************************************************************\
 * Méthodes privées
\************************************************************/

/**
 * Retourne le chemin vers le dossier image
 * @return string path
 */
function getImageDir(){
	$if(!$idTicket){
		return TARGET.'/'.ceil($idTicket / 1000);
	} else {
		return "idTicket empty";
	}
}