<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['valide'])) {

        $id_ticket = $_GET['ticketid'];

        valide($id_ticket, $bdd);

    }
}

/**
 * Permet d'obtennir le formulaire de validation du ticket
 * @param  int $classe_user		Classe de l'utilisateur, permet de déterminer s'il a accès à ce formulaire ou non
 * @return string		Retourne mes éléments HTML permettant d'afficher le formulaire de suppression d'un ticket
 */
function getValidation($classe_user) {

    $sup = "";

    if ($classe_user == 1 ) {

        $sup = '<div class="sup">'
            . '<h2>'."Validation un ticket".'</h2>'
            . '<p>'."En cliquant sur ce bouton vous Valider définitivement ce ticket.".'</p>'
            . '<form method="post" onsubmit="return confirm(\'Voulez vous réellement Valider ce ticket ?\');">'
            . '<input type="submit" class="button details" name="valide" value="Valider le ticket">'
            . '</form>'
            . '</div>';
    }

    return $sup;
}


/**
 * Permet de supprimer un ticket
 * @param  int $id_ticket	Identifiant du ticket à supprimer
 * @param  PDO $pdo			Instance de l'objet PDO permettant de dialoguer avec la base de donnée
 */
function valide($id_ticket, PDO $pdo) {

    include_once dirname(dirname(__FILE__)).'/uri.php';
    include_once dirname(dirname(__FILE__)).'/php/send_mail.php';
    include_once dirname(dirname(__FILE__)).'/php/var_mail.php';

    $var_mail = new var_mail();

    $sql = "SELECT  * FROM `TICKET` WHERE `ID` = ".$id_ticket.";";

    $res = $pdo->query($sql);
    $info = $res->fetchAll(PDO::FETCH_ASSOC);

    $titre = $info[0]['TITRE'];
    $creatuer = getUserFromPost($id_ticket,$pdo)[0];

    $fromEmail = $creatuer['MAIL'];
    $fromName = $creatuer['NOM'] . ' ' . $creatuer['PRENOM'];

    $error = sendMail('Atelier',$var_mail->VAR_MAIL_sender,$fromEmail,$fromName,$titre,2);

    if ($error == ''){
        $sql = "UPDATE `TICKET` SET `VALIDER` = '1' WHERE `ID` = '".$id_ticket."';";
        $pdo->exec($sql);

        $_SESSION['msg'] = "Le ticket à correctement été valider";
    }else{
        $_SESSION['msg'] = "Une erreur est survenue !<br>" . $error;
    }

    unset($_GET);

    header('location: '.geturi().'/accueil.php');

}
