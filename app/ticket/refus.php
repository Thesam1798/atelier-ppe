<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['refus'])) {

        $id_ticket = $_GET['ticketid'];

        refus($id_ticket, $bdd,$_POST);

    }
}

/**
 * Permet d'obtennir le formulaire de validation du ticket
 * @param  int $classe_user		Classe de l'utilisateur, permet de déterminer s'il a accès à ce formulaire ou non
 * @return string		Retourne mes éléments HTML permettant d'afficher le formulaire de suppression d'un ticket
 */
function getRefus($classe_user) {

    $sup = "";

    if ($classe_user == 1 ) {

        $sup = '<div class="sup">'
            . '<h2>'."Refus d'un ticket".'</h2>'
            . '<p>'."En cliquant sur ce bouton vous Refusez définitivement ce ticket.".'</p>'
            . '<form method="post" onsubmit="return confirm(\'Voulez vous réellement Refusez ce ticket ?\');">'
            . '<label for="refuscomm">'."Rédiger un commentaire sur le refus :".'</label>'
            . '<textarea name="refuscomm" id="refuscomm" placeholder="Information sur le refus" required>'
            . '</textarea>'
            . '<input type="submit" class="button details" name="refus" value="Refuser le ticket">'
            . '</form>'
            . '</div>';
    }

    return $sup;
}


/**
 * Permet de supprimer un ticket
 * @param  int $id_ticket	Identifiant du ticket à supprimer
 * @param  PDO $pdo			Instance de l'objet PDO permettant de dialoguer avec la base de donnée
 */
function refus($id_ticket, PDO $pdo,$post) {

    include_once dirname(dirname(__FILE__)).'/uri.php';
    include_once dirname(dirname(__FILE__)).'/php/send_mail.php';
    include_once dirname(dirname(__FILE__)).'/php/var_mail.php';

    $var_mail = new var_mail();

    $sql = "SELECT  * FROM `TICKET` WHERE `ID` = ".$id_ticket.";";

    $res = $pdo->query($sql);
    $info = $res->fetchAll(PDO::FETCH_ASSOC);

    $titre = $info[0]['TITRE'];
    $creatuer = getUserFromPost($id_ticket,$pdo)[0];

    $fromEmail = $creatuer['MAIL'];
    $fromName = $creatuer['NOM'] . ' ' . $creatuer['PRENOM'];

    $texte = $post['refuscomm'];

    $error = sendMail('Atelier',$var_mail->VAR_MAIL_sender,$fromEmail,$fromName,$texte,3);

    if ($error == ''){
        $sql = "UPDATE `TICKET` SET `VALIDER` = '1' WHERE `ID` = '".$id_ticket."';";
        $pdo->exec($sql);

        $sql = "UPDATE `TICKET` SET `NUM_STATUT` = '5', `D_CLOTURE` = CURDATE() WHERE `ID` = $id_ticket;";
        $pdo->exec($sql);

        $_SESSION['msg'] = "Le ticket à correctement été refuser";
    }else{
        $_SESSION['msg'] = "Une erreur est survenue !";
    }

    unset($_GET);

    header('location: '.geturi().'/accueil.php');

}
