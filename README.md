# Bienvenue à Atelier PPE!

Application de gestion de ticket, pour les agent du lycée merleau-ponty (Rochefort)


# Files

Le dossier App contiens les fichier de l’application (peut être utiliser conne dossier public).
Le dossier info, a tout les info pour les test.
Le dossier doc est un réplica de la documentation pressente dans app/doc.

# Data Base

Pour modifier les connexion a la basse de donnée, il faut éditer le fichier `app/php/t_connex_bd.php`.

Il est possible de m’être a zéro ou installer tout la basse de donnée en passent par une URL : `http://your_web_server/bd/reset_db.php?verify`
